import React,{Fragment, useState, useEffect} from 'react';
import Header from './components/Header'
import Formulario from './components/Formulario'
import Informacion from './components/Informacion'
import Error from './components/Error'


function App() {

  const [consulta, setConsulta] = useState(false);
  const [cod, setCod] = useState(false)
  const [resultado, setResultado] = useState({});
  const [formulario, setFormulario] = useState({
    ciudad: '',
    pais: ''
  });

  const {ciudad, pais } = formulario;
  
  useEffect(() => {
    const consultarApi = async() =>{
      
      if (consulta) {
        const apiKey = "48ae1ecf320a0b84ee2d3b24486102e2"
      const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${apiKey}`
      
      const respuesta = await fetch(url);
      const resultado = await respuesta.json(); 
      setResultado(resultado);
      setConsulta(false);
      
      
      }
      
    }

    consultarApi();

    if (resultado.cod === "404") {
      setCod(true);
    }else{
      setCod(false)
    }
    // eslint-disable-next-line
  }, [consulta])
  
  let componente;
  if (cod) {
    componente = <Error mensaje="Ciudad no encontrada" />
  }else{
    componente = <Informacion resultado = {resultado} />
  }

  return (
    <Fragment>
      <Header
        titulo ={"React clima"}
      />

      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              
              <Formulario
                formulario = {formulario}
                setFormulario = {setFormulario}
                setConsulta = {setConsulta}
              />
            
            </div>
            <div className="col m6 s12">
              
              {componente}
            
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
