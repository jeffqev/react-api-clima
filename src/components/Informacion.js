import React from 'react';
import PropTypes from 'prop-types';
const Informacion = ({resultado}) => {

    const {name, main} = resultado
    const kelvin = 273.15;
    if (!name) return null;
    return ( 
        <div className="card-panel white col s12">
            <div className="black-text">
                <h2>Clima de {name} es: </h2>
                <p className="temperatura">
                    {parseFloat(main.temp - kelvin,10).toFixed(2)}
                    <span>&#x2103;</span>
                </p>

                <p>Temperatura maxima: 
                    {parseFloat(main.temp_max - kelvin,10).toFixed(2)}
                    <span>&#x2103;</span>
                </p>
                <p>Temperatura minima: 
                    {parseFloat(main.temp_min - kelvin,10).toFixed(2)}
                    <span>&#x2103;</span>
                </p>
                

            </div>

        </div>
     );
}

Informacion.propTypes ={
    resultado: PropTypes.object.isRequired,
}
 
export default Informacion;