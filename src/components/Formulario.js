import React,{useState} from 'react';
import Error from './Error'
import PropTypes from 'prop-types'

const Formulario = ({formulario, setFormulario,setConsulta}) => {

    const [error, setError] = useState(false)
    const {ciudad, pais} = formulario;

    const handleChange = e =>{
        setFormulario({
            ...formulario,
            [e.target.name] : e.target.value
        })
    }

    const handleSubmit = e =>{
        e.preventDefault();
        if (ciudad.trim() === '' || pais.trim()==='') {
            setError(true);
            return
        }

        setError(false);
        setConsulta(true);

    }

    

    return ( 
        <form 
            onSubmit={handleSubmit}
        >
            {error ? <Error mensaje={"Todos los campos son obligatorios"}/> : null}
            <div className="input-field col s12">
                <input 
                    type="text"
                    name = "ciudad"
                    id = "ciudad"
                    value={ciudad}
                    onChange={handleChange}
                    
                />
                <label htmlFor="ciudad">Ciudad</label>
            </div>

            <div className="input-field col s12">
                <select 
                    name="pais" 
                    id="pais"
                    value={pais}
                    onChange={handleChange}
                >   
                    <option value="">-- Seleccione un pais --</option>
                    <option value="US">Estados Unidos</option>
                    <option value="EC">Ecuador</option>
                    <option value="MX">México</option>
                    <option value="AR">Argentina</option>
                    <option value="CO">Colombia</option>
                    <option value="CR">Costa Rica</option>
                    <option value="ES">España</option>
                    <option value="PE">Perú</option>
                </select>
                <label htmlFor="pais">Pais</label>
            </div>
            <div className="input-field col s12">
                <button 
                    type="submit" 
                    className="waves-effect waves-light btn-large btn-block yellow accent-4"
                >
                    Aceptar
                </button>
            </div>
        </form>

     );
}

Formulario.propTypes = {
    formulario: PropTypes.object.isRequired,
    setFormulario: PropTypes.func.isRequired,
    setConsulta: PropTypes.func.isRequired
}

export default Formulario;